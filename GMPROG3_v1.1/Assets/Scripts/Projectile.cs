﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public int m_damage;
	public Unit m_caster;

	void OnTriggerEnter(Collider other) {
		Unit unit = other.gameObject.GetComponent<Unit> ();
		//Dont deduct if it hits the caster's collider
		if(unit != m_caster)
			unit.DeductHealth (m_damage);
		Destroy (gameObject);
	}
}
